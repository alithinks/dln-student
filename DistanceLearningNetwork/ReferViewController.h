//
//  ReferViewController.h
//  eride
//
//  Created by Ali Abdul Jabbar on 26/01/2016.
//  Copyright © 2016 Ali Abdul Jabbar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReferViewController : UIViewController

@property (strong, nonatomic) NSString *titleString;

@end
