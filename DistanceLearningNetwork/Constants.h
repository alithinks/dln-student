//
//  Constants.h
//  eride
//
//  Created by Ali Abdul Jabbar on 27/01/2016.
//  Copyright © 2016 Ali Abdul Jabbar. All rights reserved.
//

#ifndef Constants_h
#define Constants_h


#define OK @"OK"

#define eride_color [UIColor colorWithRed:(39.0/255.0) green:(174.0/255.0) blue:(96.0/255.0) alpha:1.000]
#define navigationBarTextColor [UIColor whiteColor]
#define textFieldTextColor [UIColor whiteColor]
#define textFieldErrorColor [UIColor colorWithRed:0.910 green:0.329 blue:0.271 alpha:1.000]


#endif /* Constants_h */
