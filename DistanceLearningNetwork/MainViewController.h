//
//  MainViewController.h
//  eride
//
//  Created by Ali Abdul Jabbar on 20/01/2016.
//  Copyright © 2016 Ali Abdul Jabbar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGConferenceDatePicker.h"
#import "MGConferenceDatePickerDelegate.h"
#import "MBSliderView.h"

@interface MainViewController : UIViewController <MGConferenceDatePickerDelegate, MBSliderViewDelegate>

@end
