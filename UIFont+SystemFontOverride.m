//
//  UIFont+SystemFontOverride.m
//  DistanceLearningNetwork
//
//  Created by Ali Abdul Jabbar on 20/03/2016.
//  Copyright © 2016 Ali Abdul Jabbar. All rights reserved.
//

#import "UIFont+SystemFontOverride.h"

@implementation UIFont (SystemFontOverride)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"
/*
+ (UIFont *)boldSystemFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"aleo-regular-webfont.ttf" size:fontSize];
}

+ (UIFont *)systemFontOfSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"aleo-regular-webfont.ttf" size:fontSize];
}
*/
#pragma clang diagnostic pop


@end
